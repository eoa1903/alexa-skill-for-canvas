package edu.hsutx.eoa1903.handlers;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.LaunchRequest;
import com.amazon.ask.model.Response;

import java.util.Optional;

import static com.amazon.ask.request.Predicates.requestType;

public class LaunchRequestHandler implements RequestHandler{
         @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(requestType(LaunchRequest.class));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {
        String speechText = "Welcome to a Canvas Reminder sample. Please paste your access token from your canvas.";
        String repromptText = "Please kindly paste the access token from your Canvas";
        return input.getResponseBuilder()
                .withSimpleCard("TokenSession", speechText)
                .withSpeech(speechText)
                .withReprompt(repromptText)
                .build();
    }

}
