/*
 * HTTP Connection class for canvas API.
 * @author ekundayo
 * @version UI/UX
 *
 */

package main.edu.hsutx.eoa1903.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

public class HttpApiConnection {
    protected static String token;
    protected static String year;
    protected static String semester;
    protected static String idOfcourse;
    protected static JSONArray a;
    protected ArrayList <String> nameOfCourse = new ArrayList<String>();
    public ArrayList<String> coursesForSemester=new ArrayList<String>();
    protected ArrayList <String> idOfCourse = new ArrayList<String>();
    public static final ArrayList <String> assignments = new ArrayList<String>();
    public static final ArrayList <String> due_at = new ArrayList<String>();
    public UTCTimeConverter convertTime = new UTCTimeConverter();
    public static final ArrayList<String> doubleDueDate = new ArrayList<String>();
    public int j=0;


    public HttpApiConnection(){}

    /*
     * @param token String access token
     * @param year String year
     * @param semester String semester
     */

    public HttpApiConnection(String token, String year, String semester){
        this.token=token;
        this.year= "2020";
        this.semester=semester.toUpperCase();
    }

    /*
     * gets all course
     */
    public void courseGetter(){
        String url="https://hsutx.instructure.com/api/v1/courses";
        JSONArray courseGetterJson = httpConnection(url);
        forLoopCourse(courseGetterJson);
        for(int i=0; i<idOfCourse.size(); i++){
             assignmentsGetter(idOfCourse.get(i));
        }
    }

    /*
     * @param idOfcourse String name of course
     * @return assign ArrayList <String>
     */
    public ArrayList<String> assignmentsGetter(String idOfcourse){
        this.idOfcourse = idOfcourse;
        String url="https://hsutx.instructure.com/api/v1/courses/"
                    + this.idOfcourse
                    + "/assignments";
        JSONArray assignmentsGetter =httpConnection(url);
        ArrayList <String> assign = forLoopAssignment(assignmentsGetter);
        return assign;

    }

    /*
     * @param url link
     */
    public  JSONArray httpConnection(String url){
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String access = this.token;
        try{
            String urlString = url+"?access_token="+access;

            HttpGet httpget = new HttpGet(urlString);

            // Create a custom response handler
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

                @Override
                public String handleResponse(
                        final HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    HttpEntity entity;
                    if (status >= 200 && status < 300) {
                        entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;

                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }
            };

            String responseBody = httpclient.execute(httpget, responseHandler);
            String jsonText = responseBody;
            jsonText = "{ \"courses\":"+jsonText+"}";
            JSONObject obj = new JSONObject(jsonText);
            a = obj.getJSONArray("courses");


        } catch (Exception e) {
            e.printStackTrace();
        }

        return a;
    }

    public void forLoopCourse(JSONArray getter){
        String year = this.year.substring(0,2);
        String semester = this.semester.substring(0,2);
        String index = year +semester;
        for(int i = 0; i<getter.length(); i++) {
            JSONObject temp = (JSONObject) getter.get(i);
            nameOfCourse.add(String.valueOf(temp.get("name")));

            if(nameOfCourse.get(i).substring(0, 4).equals(index)) {
                idOfCourse.add(String.valueOf(temp.get("id")));
                coursesForSemester.add(String.valueOf(temp.get("name")).substring(15).replace("&","").toLowerCase());
            }
        }

    }

    public ArrayList forLoopAssignment(JSONArray getter){

        assignments.add(coursesForSemester.get(j));
        due_at.add("");
        for(int i = 0; i<getter.length(); i++) {
            JSONObject temp = (JSONObject) getter.get(i);
            assignments.add(String.valueOf(temp.get("name")) +".");
            String dueDate = String.valueOf(temp.get("due_at"));
            try {
                String convertedDate = convertTime.dateFormat(dueDate);
                //due_at.add(" Due date and time is "+convertedDate +".");
                due_at.add(convertedDate);
                doubleDueDate.add(convertedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        j++;
        assignments.add("");
        due_at.add("");
        return assignments;
    }


}
