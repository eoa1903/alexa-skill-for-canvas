/*
 * A Time conversion class.
 * @author ekundayo
 * @version UI/UX
 *
 */

package main.edu.hsutx.eoa1903.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class UTCTimeConverter {

    public UTCTimeConverter(){}

    /*
     *@param dateFormat String date
     * @return formatted date
    */
    public static String dateFormat(String dateFormat) throws ParseException {
        DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        if(!dateFormat.equals("null")) {
            Date date = utcFormat.parse(dateFormat);

            DateFormat pstFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss aa");
            //String dateString2 = pstFormat.format(new Date()).toString();
            pstFormat.setTimeZone(TimeZone.getTimeZone("CST"));

            return pstFormat.format(date);
        }
        return null;

    }

    /*
     * @return formatted date in Timezone
     */
    public static String getCurrentTimeZone(){
        TimeZone tz = Calendar.getInstance().getTimeZone();
        System.out.println(tz.getDisplayName());
        return tz.getID();
    }
}
