package edu.hsutx.eoa1903;

import com.amazon.ask.Skill;
import com.amazon.ask.SkillStreamHandler;
import com.amazon.ask.Skills;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.handler.GenericRequestHandler;
import edu.hsutx.eoa1903.handlers.CancelandStopIntentHandler;
import edu.hsutx.eoa1903.handlers.LaunchRequestHandler;
import edu.hsutx.eoa1903.handlers.MyTokenIsIntentHandler;
import edu.hsutx.eoa1903.handlers.MyYearAndSemesterIsIntentHandler;

import java.util.Optional;


public class CanvasReminderStreamHandler extends SkillStreamHandler {
    private static Skill getSkill() {
        return Skills.standard()
                .addRequestHandlers(
                        (GenericRequestHandler<HandlerInput, Optional<Response>>) new MyYearAndSemesterIsIntentHandler(),
                        (GenericRequestHandler<HandlerInput, Optional<Response>>) new MyTokenIsIntentHandler(),
                        (GenericRequestHandler<HandlerInput, Optional<Response>>) new LaunchRequestHandler(),
                        (GenericRequestHandler<HandlerInput, Optional<Response>>)  new CancelandStopIntentHandler(),
                        (GenericRequestHandler<HandlerInput, Optional<Response>>) new edu.hsutx.eoa1903.handlers.SessionEndedRequestHandler(),
                        (GenericRequestHandler<HandlerInput, Optional<Response>>) new edu.hsutx.eoa1903.handlers.HelpIntentHandler(),
                (GenericRequestHandler<HandlerInput, Optional<Response>>) new edu.hsutx.eoa1903.handlers.FallbackIntentHandler()
                )
                // Add your skill id below
                //.withSkillId("")
                .build();
    }

    public CanvasReminderStreamHandler() {
        super(getSkill());
    }


}
