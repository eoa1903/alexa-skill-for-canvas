package edu.hsutx.eoa1903.handlers;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.LaunchRequest;
import com.amazon.ask.model.Response;
import java.util.Optional;

import static com.amazon.ask.request.Predicates.requestType;

public class LaunchRequestHandler implements RequestHandler{
    @Override

    public boolean canHandle(HandlerInput input) {
                return input.matches(requestType(LaunchRequest.class));
    }
    @Override
    public Optional<Response> handle(HandlerInput input) {
                String speechText = "Welcome to a Canvas Reminder." +
                                    " I will be assisting you on how to set a reminder for your canvas Assignments." +
                                    " What is your current year and current semester of study?";
                String repromptText = "What is your current year of study and semester?";
                return input.getResponseBuilder()
                               .withSimpleCard("ReminderSession", speechText)
                                .withSpeech(speechText)
                                .withReprompt(repromptText)
                                .withShouldEndSession(false)
                                .build();
            }
        }