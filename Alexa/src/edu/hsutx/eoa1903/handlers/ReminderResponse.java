package edu.hsutx.eoa1903.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.services.reminderManagement.*;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.StringTokenizer;


public class ReminderResponse {
    public ReminderResponse(){}

    public com.amazon.ask.model.services.reminderManagement.ReminderResponse createReminder(HandlerInput input, String locale, String reminderLabel, String duedate) {

        StringTokenizer tokenizer = new StringTokenizer(duedate,"- :");
        int year = Integer.parseInt(tokenizer.nextToken("-"));
        int month= Integer.parseInt(tokenizer.nextToken("-"));
        int day=Integer.parseInt(tokenizer.nextToken(" "));
        int hour=Integer.parseInt(tokenizer.nextToken(":"));
        int minute=Integer.parseInt(tokenizer.nextToken(":"));
        int second=Integer.parseInt(tokenizer.nextToken(" "));

        SpokenText spokenText = SpokenText.builder()
                .withText(reminderLabel)
                .withLocale(locale)
                .build();

        AlertInfoSpokenInfo alertInfoSpokenInfo = AlertInfoSpokenInfo.builder()
                .addContentItem(spokenText)
                .build();

        AlertInfo alertInfo = AlertInfo.builder()
                .withSpokenInfo(alertInfoSpokenInfo)
                .build();
        System.out.println(year);
        System.out.println(month);
        System.out.println(day);
        System.out.println(hour);


        LocalDateTime triggerTime = LocalDateTime.of(year, month, day, hour, minute, second);



        Recurrence recurrence = Recurrence.builder()
                .addByDayItem(RecurrenceDay.FR)
                .withFreq(RecurrenceFreq.WEEKLY)
                .build();

        Trigger trigger = Trigger.builder()
                .withType(TriggerType.SCHEDULED_ABSOLUTE)
                .withScheduledTime(triggerTime)
                .withRecurrence(recurrence)
                .withTimeZoneId("America/Los_Angeles")
                .build();

        PushNotification pushNotification = PushNotification.builder()
                .withStatus(PushNotificationStatus.ENABLED)
                .build();

        ReminderRequest reminderRequest = ReminderRequest.builder()
                .withAlertInfo(alertInfo)
                .withRequestTime(OffsetDateTime.now())
                .withTrigger(trigger)
                .withPushNotification(pushNotification)
                .build();

        return input.getServiceClientFactory().getReminderManagementService().createReminder(reminderRequest);
    }
}
