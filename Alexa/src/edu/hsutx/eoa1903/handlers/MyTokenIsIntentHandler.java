package edu.hsutx.eoa1903.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.*;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;
import static edu.hsutx.eoa1903.handlers.MyYearAndSemesterIsIntentHandler.ACCESS_TOKEN_KEY;
import static edu.hsutx.eoa1903.handlers.MyYearAndSemesterIsIntentHandler.ACCESS_TOKEN_SLOT;

public class MyTokenIsIntentHandler implements RequestHandler {
    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("MyTokenIsIntent"));
    }
    public Optional<Response> handle(HandlerInput input) {
        Request request = input.getRequestEnvelope().getRequest();
        IntentRequest intentRequest = (IntentRequest) request;
        Intent intent = intentRequest.getIntent();
        Map<String, Slot> slots = intent.getSlots();

        // Get the token slot from user input.
        Slot myAccessTokenSlot = slots.get(ACCESS_TOKEN_SLOT);

        String speechText, repromptText;

        // Check for Access token and create output to user.
        if(myAccessTokenSlot != null) {

            // Store the user's favorite color in the Session and create response.
            String myAccessToken = myAccessTokenSlot.getValue();

            input.getAttributesManager().setSessionAttributes(Collections.singletonMap(ACCESS_TOKEN_KEY, myAccessToken));

            speechText =
                    "What's your semester and the year of study?";
            repromptText =
                    "Please, enter your year and semester of study.";

        }else {
            // Render an error since user input is out of token defined in interaction model.
            speechText = "Please provide  a valid token";
            repromptText =
                    "Kindly provide an Access token";
        }

        return input.getResponseBuilder()
                .withSimpleCard("TokenSession", speechText)
                .withSpeech(speechText)
                .withReprompt(repromptText)
                .withShouldEndSession(false)
                .build();
    }

}
