/*
 * A Call back class to create a reminder
 * @author ekundayo
 * @version UI/UX
 */

package edu.hsutx.eoa1903.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.*;

import java.util.Map;
import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;

public class CallBackIntentHandler implements RequestHandler {
    public static String choiceYes ="choice";

    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("CallBackIntent"));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {
        Request request = input.getRequestEnvelope().getRequest();
        IntentRequest intentRequest = (IntentRequest) request;
        Intent intent = intentRequest.getIntent();
        Map<String, Slot> slots = intent.getSlots();
        Slot myChoiceSlot = slots.get(choiceYes);

        String speechText= "What course would you like me to select?";
        String choice = myChoiceSlot.getValue();

        if(choice != null){
            if(choice.equals("yes")){
                return input.getResponseBuilder()
                        .withSimpleCard("ReminderSession", "What course would you like me to select?")
                        .withSpeech(speechText)
                        .withShouldEndSession(false)
                        .build();
            }
            if(choice.equals("no")){
                return input.getResponseBuilder()
                        .withSimpleCard("End Session", "Good Bye!")
                        .withSpeech("Thank you for using me as a reminder. Have you good day!")
                        .withShouldEndSession(true)
                        .build();
            }
        }
        return input.getResponseBuilder()
                .withSimpleCard("ReminderSession", "Reply with a yes i do or No i do not.")
                .withSpeech("Reply with a yes I do or No i do not.")
                .withShouldEndSession(false)
                .build();
    }
}
