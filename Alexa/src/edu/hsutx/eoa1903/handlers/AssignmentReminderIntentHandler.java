/*
 * An Assignment reminder class that triggers a create reminder.
 * @author ekundayo
 * @version UI/UX
 *
 */

package edu.hsutx.eoa1903.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.*;
import com.amazon.ask.model.services.reminderManagement.*;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.*;

import static com.amazon.ask.request.Predicates.intentName;
import static main.edu.hsutx.eoa1903.util.HttpApiConnection.*;

public class AssignmentReminderIntentHandler implements RequestHandler {

    public final static String ASSIGNMENT_SLOT="assignment";
    public final static String API_TOKEN="apiAccessToken";
    public final static String ASSIGNMENT_KEY="ASSIGNMENT";
    public  static int year=0;
    public static int month=0;
    public  static int day=0;
    public  static int hour=0;
    public static int minute =0;
    public static int second=0;
    public String dueDate;


    /*
     * @param input HandlerInput
     *
     * @rturn Optional<Response> ssml
     */
    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("AssignmentReminderIntent"));
    }

    /*
     * @param input HandlerInput
     */
    @Override
    public Optional<Response> handle(HandlerInput input) {
        Request request = input.getRequestEnvelope().getRequest();
        IntentRequest intentRequest = (IntentRequest) request;
        Intent intent = intentRequest.getIntent();
        Map<String, Slot> slots = intent.getSlots();
        String inputFound="", speechText="";

        // Get the assignment slot from user input.
        Slot myAssignmentSlot = slots.get(ASSIGNMENT_SLOT);
        if(myAssignmentSlot != null) {
            String myAssignment = myAssignmentSlot.getValue();
            Object assignmentName = myAssignment;

            for (int i=0; i<assignments.size();i++){
                if(assignments.get(i).toLowerCase().contains((CharSequence) assignmentName)){
                    System.out.println(assignments.get(i));
                    //dueDate = doubleDueDate.get(i);
                    dueDate = due_at.get(i);
                    inputFound = (String) assignmentName;
                    System.out.println(inputFound);
                    System.out.println(dueDate);
                }
            }


            input.getAttributesManager().setSessionAttributes(Collections.singletonMap(ASSIGNMENT_KEY, myAssignment));

            //check for permission
            Permissions permissions = input.getRequestEnvelope().getContext().getSystem().getUser().getPermissions();
            String locale = input.getRequestEnvelope().getRequest().getLocale();

            if(null!=permissions) {
                String reminderText = assignmentName +" Assignment";
                String callback ="Would you like to set another reminder?";
                try {
                    ReminderResponse reminderResponse = createReminder(input, locale, reminderText, dueDate);
                     speechText = "Ok, I will remind you about " +inputFound +
                                   " every day at "+hour+":"+minute+ " "+dueDate.substring(20)
                                    +"."+"\n"+"Would you like to set another reminder?";

                }
                catch (NoClassDefFoundError e){
                    speechText="No Class found";
                    System.out.println("No class found");
                }
                return input.getResponseBuilder()
                        .withSpeech(speechText)
                        .withSimpleCard("ReminderSession", speechText)
                        .withShouldEndSession(false)
                        .build();
            }
            else {
                speechText = "In order for  this skill to create a reminder, please grant permission using the card I sent to your Alexa app";
                List<String> list = new ArrayList<>();
                list.add("alexa::alerts:reminders:skill:readwrite");
                return input.getResponseBuilder()
                        .withSpeech(speechText)
                        .withAskForPermissionsConsentCard(list)
                        .withShouldEndSession(false)
                        .build();
            }
        }
        return input.getResponseBuilder()
                .withSimpleCard("ReminderSession", "Enter a valid assignment in "+inputFound+".")
                .withSpeech("What assignment would to set a reminder on?")
                .withShouldEndSession(false)
                .build();

    }

    /*
     * @param input HandlerInput
     * @param locale String value en-us
     * @pram reminderLabel String Text reminder
     * @param duedate String due date
     *
     * @return ReminderResponse
     */

    private ReminderResponse createReminder(HandlerInput input, String locale, String reminderLabel, String duedate) {
        StringTokenizer tokenizer = new StringTokenizer(duedate);
         year = Integer.parseInt(tokenizer.nextToken("-"));

         month= Integer.parseInt(tokenizer.nextToken("-"));
         day=Integer.parseInt(tokenizer.nextToken(" \\s").substring(1));

         hour=Integer.parseInt(tokenizer.nextToken(":").substring(1));

         minute=Integer.parseInt(tokenizer.nextToken(":").substring(0));

         second=Integer.parseInt(tokenizer.nextToken(" ").substring(1));

        //String r = (String) input.getAttributesManager().getSessionAttributes().get(API_TOKEN);

        SpokenText spokenText = SpokenText.builder()
                .withText(reminderLabel)
                .withLocale(locale)
                .withSsml("You have an Assignment due")
                .build();

        AlertInfoSpokenInfo alertInfoSpokenInfo = AlertInfoSpokenInfo.builder()
                .addContentItem(spokenText)
                .build();

        AlertInfo alertInfo = AlertInfo.builder()
                .withSpokenInfo(alertInfoSpokenInfo)
                .build();


        LocalDateTime triggerTime = LocalDateTime.of(year, month, day, hour, minute, second);



        Recurrence recurrence = Recurrence.builder()
                .addByDayItem(RecurrenceDay.UNKNOWN_TO_SDK_VERSION)
                .withFreq(RecurrenceFreq.DAILY)
                .withInterval(3)
                .withStartDateTime(LocalDateTime.now())
                .withEndDateTime(triggerTime)
                .build();

        Trigger trigger = Trigger.builder()
                .withType(TriggerType.SCHEDULED_ABSOLUTE)
                .withScheduledTime(triggerTime)
                .withRecurrence(recurrence)
                .withTimeZoneId("America/Chicago")
                .build();

        PushNotification pushNotification = PushNotification.builder()
                .withStatus(PushNotificationStatus.ENABLED)
                .build();

        ReminderRequest reminderRequest = ReminderRequest.builder()
                .withAlertInfo(alertInfo)
                .withRequestTime(OffsetDateTime.now())
                .withTrigger(trigger)
                .withPushNotification(pushNotification)
                .build();

        return input.getServiceClientFactory().getReminderManagementService().createReminder(reminderRequest);
    }


}
