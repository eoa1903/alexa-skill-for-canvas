package edu.hsutx.eoa1903.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.*;
import main.edu.hsutx.eoa1903.util.HttpApiConnection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;

public class MyYearAndSemesterIsIntentHandler implements RequestHandler {
    public static final String ACCESS_TOKEN_KEY = "Access_token";
    public static final String ACCESS_TOKEN_SLOT = "token";
    public static final String YEAR_SLOT="year";
    public static final String YEAR_KEY="YEAR";
    public static final String SEMESTER_SLOT="semester";
    public static final String SEMESTER_KEY="SEMESTER";

    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("MyYearAndSemesterIsIntent"));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {
        Request request = input.getRequestEnvelope().getRequest();
        IntentRequest intentRequest = (IntentRequest) request;
        Intent intent = intentRequest.getIntent();
        Map<String, Slot> slots = intent.getSlots();

        // Get the year and semester slot from user input.
        Slot myYearSlot = slots.get(YEAR_SLOT);
        Slot mySemesterSlot=slots.get(SEMESTER_SLOT);
        String speechText = null, repromptText, out="";
        String myToken = (String) input.getAttributesManager().getSessionAttributes().get(ACCESS_TOKEN_KEY);

        if(!myYearSlot.equals("") && !mySemesterSlot.equals("")) {

            String myYear = myYearSlot.getValue();
            String mySemester = mySemesterSlot.getValue();
            System.out.println("-"+myYear+"-");
            System.out.println("-"+mySemester+"-");
            input.getAttributesManager().setSessionAttributes(Collections.singletonMap(YEAR_KEY, myYear));
            input.getAttributesManager().setSessionAttributes(Collections.singletonMap(SEMESTER_KEY, mySemester));

            try {

                HttpApiConnection client = new HttpApiConnection(myToken, myYear,mySemester);
                client.courseGetter();

                ArrayList<String> courses = client.coursesForSemester;
                for (int i = 0; i < courses.size(); i++) {
                    out += "\n" + courses.get(i)+".";
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            speechText="Thanks, " +
                    "for this " +mySemester+
                    " semester, you have"+ out
                    +"\n"+"What course would you like to select?";
            repromptText="Enter the name of the course.";

        }else {

            speechText = "Please provide  a valid year and semester";
            repromptText =
                    "Kindly provide a valid year and semester";
        }

        return input.getResponseBuilder()
                .withSimpleCard("ReminderSession", speechText)
                .withSpeech(speechText)
                .withReprompt(repromptText)
                .withShouldEndSession(false)
                .build();
    }
}