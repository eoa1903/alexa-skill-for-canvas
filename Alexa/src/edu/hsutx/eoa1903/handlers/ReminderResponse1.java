/*
 * A reminder class that triggers the create reminder
 * @author ekundayo
 * @version UI/UX
 */


package edu.hsutx.eoa1903.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.services.reminderManagement.*;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.StringTokenizer;


public class ReminderResponse1 {

    private com.amazon.ask.model.services.reminderManagement.ReminderResponse createReminder(HandlerInput input, String locale, String reminderLabel, String duedate) {

        StringTokenizer tokenizer = new StringTokenizer(duedate);
        int year = Integer.parseInt(tokenizer.nextToken("-"));
        System.out.println(year +" year");
        int month= Integer.parseInt(tokenizer.nextToken("-"));
        System.out.println(month +" month");
        int day=Integer.parseInt(tokenizer.nextToken(" \\s").substring(1));
        System.out.println(day +" day");
        int hour=Integer.parseInt(tokenizer.nextToken(":").substring(1));
        System.out.println(hour +" hour");
        int minute=Integer.parseInt(tokenizer.nextToken(":").substring(1));
        System.out.println(minute +" minute");
        int second=Integer.parseInt(tokenizer.nextToken(" ").substring(1));
        System.out.println(second +" second");


        SpokenText spokenText = SpokenText.builder()
                .withText(reminderLabel)
                .withLocale(locale)
                .build();

        AlertInfoSpokenInfo alertInfoSpokenInfo = AlertInfoSpokenInfo.builder()
                .addContentItem(spokenText)
                .build();

        AlertInfo alertInfo = AlertInfo.builder()
                .withSpokenInfo(alertInfoSpokenInfo)
                .build();



        LocalDateTime triggerTime = LocalDateTime.of(year, month, day, hour, minute, second);



        Recurrence recurrence = Recurrence.builder()
                .addByDayItem(RecurrenceDay.FR)
                .withFreq(RecurrenceFreq.WEEKLY)
                .build();

        Trigger trigger = Trigger.builder()
                .withType(TriggerType.SCHEDULED_ABSOLUTE)
                .withScheduledTime(triggerTime)
                .withRecurrence(recurrence)
                .withTimeZoneId("America/Los_Angeles")
                .build();

        PushNotification pushNotification = PushNotification.builder()
                .withStatus(PushNotificationStatus.ENABLED)
                .build();

        ReminderRequest reminderRequest = ReminderRequest.builder()
                .withAlertInfo(alertInfo)
                .withRequestTime(OffsetDateTime.now())
                .withTrigger(trigger)
                .withPushNotification(pushNotification)
                .build();

        return input.getServiceClientFactory().getReminderManagementService().createReminder(reminderRequest);
    }
}
