/*
 * A class that shows the course of the semester
 * @author ekundayo
 * @version UI/UX
 */

package edu.hsutx.eoa1903.handlers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;
import static main.edu.hsutx.eoa1903.util.HttpApiConnection.assignments;
import static main.edu.hsutx.eoa1903.util.HttpApiConnection.due_at;


public class DisplayNameOfCourseIntentHandler implements RequestHandler {
    public static final String COURSE_SLOT="course";
    public static final String COURSE_KEY="COURSE";

    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("DisplayNameOfCourseIntent"));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {
        Request request = input.getRequestEnvelope().getRequest();
        IntentRequest intentRequest = (IntentRequest) request;
        Intent intent = intentRequest.getIntent();
        Map<String, Slot> slots = intent.getSlots();
        String speechText = null, repromptText=null, out="";

        // Get the course slot from user input.
        Slot myCourseSlot = slots.get(COURSE_SLOT);


        if(myCourseSlot != null) {
            String myCourse = myCourseSlot.getValue();
            input.getAttributesManager().setSessionAttributes(Collections.singletonMap(COURSE_KEY, myCourse));
            Object courseName = myCourse;

            try {

                ArrayList<String> string = assignments;
                ArrayList<String> due_date = due_at;

                System.out.println(string.size()+"  string size");
                System.out.println(due_at.size()+"  due_at size");


                for (int i = 0; i < string.size(); i++) {
                    if(string.get(i).contains((CharSequence) courseName)){
                        int j=1;
                        while(!(string.get(i+j).equals("") && due_at.get(i+j).equals(""))){
                            out += "\n" + string.get(i+j)+due_at.get(i+j);
                            j++;
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            speechText="Alright, what assignment in " + myCourse
                            +" would you want me to set a reminder on?";
            repromptText="Enter the name of your Homework to set a reminder.";
        }
        else{
            speechText="Course name is invalid, please try again.";
            repromptText="Enter a valid course name.";

        }


        return input.getResponseBuilder()
                .withSimpleCard("ReminderSession", speechText)
                .withSpeech(speechText)
                .withReprompt(repromptText)
                .withShouldEndSession(false)
                .build();
    }


}
