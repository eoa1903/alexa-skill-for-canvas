package helloworld;

import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

/**
 * Handler for requests to Lambda function.
 */
public class App implements RequestHandler<Object, Object> {

    public Object handleRequest(final Object input, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("X-Custom-Header", "application/json");
        try {
            final String pageContents = this.getPageContents("https://checkip.amazonaws.com");
            String output = String.format("{ \"message\": \"hello world\", \"location\": \"%s\" }", pageContents);
            return new GatewayResponse(output, headers, 200);
        } catch (IOException e) {
            return new GatewayResponse("{}", headers, 500);
        }
    }

    private String getPageContents(String address) throws IOException{
        URL url = new URL(address);
        try(BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()))) {
            return br.lines().collect(Collectors.joining(System.lineSeparator()));
        }
    }
    
    public void urlReader(String token) throws IOException {
        String access_token=token;
        
        String urlString = "https://hsutx.instructure.com/api/v1/courses?access_token=" + access_token;


        URL url = new URL(urlString);

        // open the url stream, wrap it an a few "readers"
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));

        // write the output to stdout
        String line;
        String output = null;
        while ((line = reader.readLine()) != null)
        {
            output += line + "\n";

        }
        // close our reader
        reader.close();
        
        //store the json file into

        File aFile = new File("token.json");
        FileWriter aFilewriter = new FileWriter(aFile);
        PrintWriter aPrinterwriter = new PrintWriter(aFilewriter);
        aPrinterwriter.println(output);
        aPrinterwriter.close(); //close file
        
    }
}
